<?php 
	session_start();
	include "koneksi.php";
	error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Maudhio Andre | UJIKOM | INVENTARIS SEKOLAH </title>
	<link rel="icon" href="">
  	<meta charset="utf-8">
  	<meta name="keywords" />
  	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes">
  	<!-- CSS Bootstrap -->
  	<link rel="stylesheet" type="text/css" href="bt4/css/bootstrap.min.css">
  	<link rel="stylesheet" type="text/css" href="bt4/css/style.css">
  	<!-- JS Bootstrap -->
  	<script type="text/javascript" src="bt4/js/jquery.js"></script>
  	<script type="text/javascript" src="bt4/js/bootstrap.min.js"></script>
  	<script type="text/javascript" src="bt4/js/jquery-1.11.1.min.js"></script>
  	<!-- Data Table -->
  	<link rel="stylesheet" type="text/css" href="data_table/assets/css/jquery.dataTables.css">
  	<!-- CSS PRIBADI -->
  	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/login.css">

</head>
<body background="img/login.jpg">

<div class="cont">
  <div class="demo">
    <div class="login">
      <div class="login__check"></div>
      <div class="login__form" style="margin-top: -35px">
        <form action="" method="POST">
        <div class="login__row">
          <svg class="login__icon name svg-icon" viewBox="0 0 20 20">
            <path d="M0,20 a10,8 0 0,1 20,0z M10,0 a4,4 0 0,1 0,8 a4,4 0 0,1 0,-8" />
          </svg>
          <input type="text" class="login__input name" placeholder="Username" 
          maxlength="20" autocomplete="off" name="username"  autofocus="" required />
        </div>
        <div class="login__row">
          <svg class="login__icon pass svg-icon" viewBox="0 0 20 20">
            <path d="M0,20 20,20 20,8 0,8z M10,13 10,16z M4,8 a6,8 0 0,1 12,0" />
          </svg>
          <input type="password" class="login__input pass" placeholder="Password" 
          maxlength="20" autocomplete="off" name="password" required/>
        </div>
        <div>
		<select class="login__input name" name="level" id="hak__akses" >
	          <option selected="selected">PILIH AKSES</option>
	          <option value="admin" style="color: black">ADMIN</option>
	          <option value="operator" style="color: black">OPERATOR</option>
	    </select>
        </div>
        <button type="submit" name="submit" class="login__zzz">Login</button>
    	</form>
        <p class="login__signup">Masuk sebagai peminjam? &nbsp;<a href="login2.php">Klik disini</a></p>
      </div>
    </div>
  </div>
</div>


<?php
				if(isset($_POST['submit'])){	
				include "koneksi.php";

					$u	= $_POST['username'];
					$p	= $_POST['password'];
					$level	= $_POST['level'];
					
					$query =$koneksi->query("SELECT * FROM petugas JOIN level ON petugas.id_lvl=level.id_level WHERE username='$u' AND password='$p' ");
					
					$anjay=$query->num_rows;
					if($anjay == 0){
						?>
              <script type="text/javascript">
                      alert("Login gagal, Username atau Password salah");
              </script>
          <?php					
        }else{
						$row = $query->fetch_assoc();
						
						if($row['nama_level'] == 'admin' && $level == 'admin'){
							$_SESSION['id_pengguna']=$row['id_petugas'];
							$_SESSION['nama_level']='admin';
              $_SESSION['nama_pengguna']=$row['nama_petugas'];
							header("Location:!admin/index.php");
						}else if($row['nama_level'] == 'operator' && $level == 'operator'){
              $_SESSION['id_pengguna']=$row['id_petugas'];
							$_SESSION['nama_level']='operator';
              $_SESSION['nama_pengguna']=$row['nama_petugas'];
							header("Location:!operator/index.php");
						}
					}
				}
?>

	<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  	<script  src="assets/js/index.js"></script>

<?php 
include "footer.php";
?>


	

