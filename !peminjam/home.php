<script src="assets/Chart.js/Chart.bundle.js"></script>

<p class="title-content"><b>BERANDA</b></p>
<div id="hor-line"></div>
<?php 
include "../koneksi.php";
include "function.php";
$db= new database();
?>
    <div class="row" style="margin-left: 0px; margin-right: 0px;">
    <?php
    $a=$db->tampil_barang(); 
    ?>
    <div class="col-md-4" style="background-color: grey; padding-top: 10px; padding-bottom: 10px; color:white;">
        <p align="center" style="color: white;"><?php echo $a['pinjam']; ?> Barang<br> Sedang Dipinjam</p>
    </div>

    <?php
    $a=$db->tampil_barang2(); 
    ?>
    <div class="col-md-4" style="background-color: darkgrey; padding-top: 10px; padding-bottom: 10px; color:red;">
        <p align="center" style="color: white;"><?php echo $a['barang']; ?> Total<br> Barang</p>
    </div>
    
    <?php
    $a=$db->tampil_ruang(); 
    ?>
    <div class="col-md-4" style="background-color: black; padding-top: 10px; padding-bottom: 10px; color:red;">
        <p align="center" style="color: white;"><?php echo $a['ruang']; ?> Total<br> Ruangan</p>
    </div>
    </div>

<div class="row" style="margin-top: 30px;">    
<div class="col-md-5" >
    <canvas id="myChart" width="100" height="70"></canvas>
</div>

<div class="col-md-5 offset-1" >
    <canvas id="myChart2" width="100" height="70"></canvas>
</div>
</div>
<!-- LIST CHARTS
line, bar, radar, doughnut, pie, polar area, bubble, scatter, area, mixed -->

<?php 
$query=$koneksi->query("SELECT jumlah AS ju FROM inventaris");
$query2=$koneksi->query("SELECT * FROM inventaris");
?>


<script>
            var ctx = document.getElementById("myChart");
            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels:[<?php while ($b = $query2->fetch_array()) { echo '"' . $b['nama'] . '",';}?>],
                    datasets: [{
                            label:'Daftar List Jumlah Barang',
                            data: [<?php while ($c = $query->fetch_array()) { echo "$c[ju],";}?>],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)'
                            ],
                            borderWidth: 1
                        }]
                },
                options: {
                    scales: {
                        yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                    }
                }
            });
</script>


<script>
            var ctx = document.getElementById("myChart2");
            var myChart = new Chart(ctx, {
                type: 'pie',
                data: {
                    labels:['SEDANG DIPINJAM','TELAH DIKEMBALIKAN'],
                    datasets: [{
                            label:'Daftar List Jumlah Barang',
                            data: ['23','45'],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)'
                            ],
                            borderWidth: 1
                        }]
                },
                options: {
                    scales: {
                        yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                    }
                }
            });
</script>