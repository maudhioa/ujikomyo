<?php 
	include "../koneksi.php";
	error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
	session_start();
	if ($_SESSION['identified']!='peminjam') {
		header("location:../login2.php");
	}
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Maudhio Andre | UJIKOM | INVENTARIS SEKOLAH </title>
  <link rel="icon" href="">
  <meta charset="utf-8">
  <meta name="keywords" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- CSS Bootstrap -->
  <link rel="stylesheet" type="text/css" href="assets/bt4/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="assets/bt4/css/style.css">
  <!-- JS Bootstrap -->
  <script type="text/javascript" src="assets/bt4/js/jquery.js"></script>
  <script type="text/javascript" src="assets/bt4/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="assets/bt4/js/jquery-1.11.1.min.js"></script>
  <script type="text/javascript" src="assets/js/profile.js"></script>
  <!-- Data Table -->
  <link rel="stylesheet" type="text/css" href="assets/data_table/assets/css/jquery.dataTables.css">
  <!-- CSS Pribadi -->
  <link rel="stylesheet" type="text/css" href="assets/css/style4.css">
  <link rel="stylesheet" type="text/css" href="assets/css/style.css">
  <link rel="stylesheet" type="text/css" href="assets/css/form-wizard.css">
  <link rel="stylesheet" type="text/css" href="assets/css/profile.css">

</head>
<body>

<body>

    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>Skanic Inventaris</h3>
                <strong>SI</strong>
            </div>

            <ul class="list-unstyled components">
                <li class="">
                    <a href="?page=&aksi=">
                        <img src="img/home.png" class="img-menu">
                        Beranda
                    </a>
                </li>
                                
                <li>
                    <a href="?page=pengembalian">
                        <img src="img/brief.png" class="img-menu">
                        Pengembalian
                    </a>
                </li>
                <li>
                    <a href="?page=pinjam">
                        <img src="img/book.png" class="img-menu">
                        Pinjam Barang
                    </a>
                </li>                
                
                <li>
                    <!--<a href="#">
                        <img src="img/news.png" class="img-menu">
                        Laporan
                    </a>-->
                </li>
            </ul>

            <!--<ul class="list-unstyled CTAs">
                <li>
                    <a href="https://bootstrapious.com/tutorial/files/sidebar.zip" class="download">Download source</a>
                </li>
                <li>
                    <a href="https://bootstrapious.com/p/bootstrap-sidebar" class="article">Back to article</a>
                </li>
            </ul>-->
        </nav>




        <!-- Page Content  -->
        <div id="content">


            <nav class="navbar navbar-expand-lg navbar-light bg-light" style="margin-left: -20px;margin-right: -20px;">
                <div class="container-fluid">
                  <a id="sidebarCollapse" style="margin-left: -1%;" >
                        <i class="fas fa-align-left"><img src="../img/sidebar-icon.png"></i>
                    </a>
                    
                        
                  <form class="navbar-form navbar-right" >


                    <div class='profile' style="margin-right: 1%;">
                      <div class='profile__name i-block pull-left' 
                      style="font-size: 18px; color: #5A738E">
                        <?php 
                        session_start();
                        echo $_SESSION['nama_pengguna']; ?></div>
                      <ul class='menu'>
                        <a href="?page=edit_profile"><li class='menu__item'>Profile</li></a>
                        <!--<li class='menu__item'>Pesan</li>-->
                        <a href="?page=pengaturan_akun"><li class='menu__item'>Pengaturan</li></a>
                        <a href="logout.php"><li class='menu__item'>Log out</li></a>
                      </ul>
                    </div>
                  </form>

                </div>
            </nav>

            <div class="zzz">
            <?php 
              $page = $_GET['page'];

              if ($page=='') {
                  include "home.php";
              }
              if ($page=='pinjam') {
                include "page/pinjam/pinjam.php";
              }
              if ($page=='edit_profile') {
                include "page/akun/edit_profile.php";
              }
              if ($page=='pengaturan_akun') {
                include "page/akun/pengaturan.php";
              }
              if ($page=='pengembalian') {
                if (isset($_GET['kej'])) {
                  $kej = $_GET['kej'];
                  if ($kej == '') {
           
                  }
                }else{
                    include "page/pengembalian/pengembalian2.php";
                }
              }
            ?>
            </div>

        </div>
    </div>

<?php 
include "footer.php";
 ?>