<?php 
include "function.php";
$tampil = new database();
?>
<p class="title-content"><b>Pengembalian Barang</b></p>
<div id="hor-line"></div>


<nav class="nav nav-tabs" id="myTab" role="tablist" style="margin-top: 20px; font-size: 16px; margin-bottom: 15px;">
  <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Pengembalian</a>
  <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Pengembalian Lengkap</a>
</nav>


<div class="tab-content" id="nav-tabContent">
  <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
  	<a href="?page=pengembalian&kej=lengkap" class="btn btn-info" style="margin-bottom: 10px;">LENGKAP</a>
  	<div class="table-responsive">
			<table class="table table-bordered table-hover" id="example">
				<thead>
					<tr>
						<th>No</th>
						<th>Nama Peminjam</th>
						<th>Level</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$no=1;
					$a = $tampil->tampil_pengembalian("SEDANG DIPINJAM"); 
					foreach ($a as $data) {
						?>
						<tr>
							<td><?php echo $no ?></td>	
							<td><?php echo $data['nama_peminjam'] ?></td>	
							<td><?php echo $data['level'] ?></td>	
							<td>
								<?php echo "
								<a class='btn btn-danger' style='color:white' href='?page=pengembalian&kej=kembalikan&id_peminjam=$data[id_peminjam]'>LIHAT BARANG</a> " 
								?>
							</td>	
						</tr>
						<?php
						$no++;
					}
					?>
				</tbody>
			</table>
		</div>
  </div>

  <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
  	<div class="table-responsive">
			<table class="table table-bordered table-hover" id="example">
				<thead>
					<tr>
						<th>No</th>
						<th>Kode</th>
						<th>Nama</th>
						<th>Level</th>
						<th>Barang</th>
						<th>Jumlah Pinjam</th>
						<th>Tgl Pinjam</th>
						<th>Tgl Kembali</th>
						<th>Status</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$no=1;
					$a = $tampil->pengembalian_lengkap("TELAH DIKEMBALIKAN"); 
					foreach ($a as $data) {
						?>
						<tr>
							<td><?php echo $no ?></td>	
							<td><?php echo $data['kode_peminjaman'] ?></td>	
							<td><?php echo $data['nama_peminjam'] ?></td>	
							<td><?php echo $data['level'] ?></td>	
							<td><?php echo $data['nama'] ?></td>	
							<td><b><?php echo $data['jumlah_pinjam'] ?></b></td>	
							<td><?php echo $data['tanggal_pinjam'] ?></td>	
							<td><?php echo $data['tanggal_kembali'] ?></td>	
							<td><?php echo $data['status_peminjaman'] ?></td>	
							<td>
								<?php echo "
								<a class='btn btn-danger' style='color:white' href='page/pengembalian/proses.php?aksi=kembalikan&kode_peminjaman=$data[kode_peminjaman]&id_peminjam=$id'>HAPUS</a> " 
								?>
							</td>	
						</tr>
						<?php
						$no++;
					}
					?>
				</tbody>
			</table>
		</div>
  </div>
</div>

