<?php 
session_start();
include "../koneksi.php";
$id_inventaris=$_GET['id_inventaris'];
$query=$koneksi->query("SELECT * FROM inventaris WHERE id_inventaris=$id_inventaris");
$data=$query->fetch_assoc();
?>

<p class="title-content"><b>Tambah Data Barang / Inventaris</b></p>
<div id="hor-line"></div>

<form class="form-horizontal" method="POST" action="page/barang/proses.php?aksi=ubah" enctype="multipart/form-data"> 
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label>Nama Barang * :</label>
				<input type="text" value="<?php echo $data['nama'] ?>" name="nama_barang" class="form-control" autocomplete="off" required>
				<input type="hidden" value="<?php echo $data['id_inventaris'] ?>" name="id_barang">
				<input type="hidden" value="<?php echo $_SESSION['id_pengguna'] ?>" name="id_petugas">
			</div>
			<div class="form-group">
				<label>Kode * :</label>
				<input type="text" value="<?php echo $data['kode_inventaris'] ?>" name="kode" class="form-control" autocomplete="off" required>
			</div>
			<div class="form-group">
				<label>Jumlah * :</label>
				<input type="number" value="<?php echo $data['jumlah'] ?>" name="jumlah" class="form-control" autocomplete="off" required>
			</div>
			<div class="form-group">
				<label>Kondisi * :</label>
				<select name="kondisi" class="form-control">
					<option <?php if($data['kondisi']=='Buruk'){ echo "selected";} ?> >Buruk</option>
					<option <?php if($data['kondisi']=='Baik'){ echo "selected";} ?> >Baik</option>
					<option <?php if($data['kondisi']=='Sangat Baik'){ echo "selected";} ?> >Sangat Baik</option>
				</select>
			</div>
	
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label>Ruang * :</label>
				<select class="form-control" name="ruang" required>
					<option value="1" <?php if($data['id_ruang']=="1")echo " selected"; ?>>Lapangan</option>                 
					<option value="2" <?php if($data['id_ruang']=="2")echo " selected"; ?>>Lab Rpl1</option>                 
				</select>
			</div>
			<div class="form-group">
				<label>Jenis * :</label>
				<select class="form-control" name="jenis" required>
					<option value="1" <?php if($data['id_jenis']=="1")echo " selected"; ?>>Alat Olahraga</option>                 
					<option value="2" <?php if($data['id_jenis']=="2")echo " selected"; ?>>Alat Elektronik</option>
					<option value="3" <?php if($data['id_jenis']=="3")echo " selected"; ?>>Alat Tetap</option>
					<option value="4" <?php if($data['id_jenis']=="4")echo " selected"; ?>>Pembelajaran</option>
				</select>
			</div>
				<label>Keterangan :</label>
				<textarea class="form-control" name="keterangan"><?php echo $data['keterangan']; ?></textarea>
				<label>Foto/Gambar * :</label>
				<input class="form-control" type="file" name="foto">
		</div>
	<button type="submit" class="btn btn-success">Ubah</button>
	<button type="button" class="btn btn-danger"><a href="?page=barang">kembali</a></button>
</form>