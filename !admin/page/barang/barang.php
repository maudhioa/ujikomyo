<?php 
	include "function.php";
	$z = new database();
	$a = $z->tampil_barang();
?>

<p class="title-content"><b>Tabel Data Barang / Inventaris</b></p>
<div id="hor-line"></div>

<div class="row">
	<div class="col-md-9">
<a href="?page=barang&kej=tambah" class="btn btn-success" style="margin-bottom: 15px;"><b>+</b> Tambah Barang</a> 
</div>
<p align="right">Buat Laporan :  <a href="page/barang/lap_excel.php" class="btn btn-dark">Ex</a>
<a href="page/barang/lap_pdf.php" class="btn btn-dark">Pdf</a></p>
</div>

<?php 
include "../koneksi.php";
$modal=$koneksi->query("SELECT * FROM inventaris");
while ($sq = $modal->fetch_assoc()) 
{
	?>
	<div class="modal fade" id="detail<?=$sq['id_inventaris'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" style="max-width: 700px;">
			<div class="modal-content">
				<div class="modal-header">
					<h3><b>Data Lengkap Inventaris/Barang</b></h3><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<div class="table-responsive">
						<table class="table table-bordered table-hover">
							<thead></thead>
							<tbody>
								<?php
								$dataa=$koneksi->query("SELECT * FROM inventaris i JOIN ruang r ON i.id_ruang=r.id_ruang JOIN jenis j ON i.id_jenis=j.id_jenis JOIN petugas p ON i.id_petugas=p.id_petugas WHERE id_inventaris=$sq[id_inventaris]" );
								$no=1;
								while ($ra=$dataa->fetch_assoc())
								{	

									echo"<tr>";
									echo "<td rowspan=6>
									<img src='page/barang/img/$ra[foto]' width=200px></td>";
									echo"</tr>";

									echo"<tr>";
									echo '<th>Nama Barang</th>';
									echo "<td>$ra[nama]</td>";
									echo '<th>Jenis</th>';
									echo "<td>$ra[nama_jenis]</td>";
									echo"</tr>";

									echo"<tr>";
									echo'<th>Kode</th>';
									echo "<td>$ra[kode_inventaris]</td>";
									echo'<th>Ruang</th>';
									echo "<td>$ra[nama_ruang]</td>";
									echo"</tr>";

									echo"<tr>";
									echo'<th>Jumlah</th>';			
									echo "<td>$ra[jumlah]</td>";
									echo'<th>Petugas</th>';			
									echo "<td>$ra[nama_petugas]</td>";
									echo"</tr>";

									echo"<tr>";
									echo'<th>Kondisi</th>';			
									echo "<td>$ra[kondisi]</td>";
									echo'<th>Tanggal Register</th>';			
									echo "<td>$ra[tanggal_register]</td>";
									echo"</tr>";

									echo"<tr>";
									echo'<th>Keterangan</th>';			
									echo "<td colspan=3>$ra[keterangan]</td>";
									echo"</tr>";

									echo"<tr>";
									echo"<th colspan=5><a class='btn btn-dark' style='color:white' href='?page=barang&kej=ubah&id_inventaris=$ra[id_inventaris]'>Edit</a></th>";			
									echo"</tr>";
									$no++;
								}
								?>
							</tbody>
						</table>
					</div>		
				</div>
			</div>
		</div>
	</div>
<?php } ?>





<div class="table-responsive">
	<table class="table table-bordered table-hover" id="example">
		<thead>
			<tr>
				<th>No</th>
				<th>Nama Barang</th>
				<th>Kode</th>
				<th>Jumlah</th>
				<th>Kondisi</th>
				<th>Ruang</th>
				<th>Jenis</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
		<?php
		$no=1; 
		foreach ($a as $data) {
		?>
		<tr>
			<td><?php echo $no ?></td>	
			<td><?php echo $data['nama'] ?></td>	
			<td><?php echo $data['kode_inventaris'] ?></td>	
			<td><?php echo $data['jumlah'] ?></td>	
			<td><?php echo $data['kondisi'] ?></td>	
			<td><?php echo $data['nama_ruang'] ?></td>	
			<td><?php echo $data['nama_jenis'] ?></td>	
			<td>
				<?php echo "<a data-toggle='modal' data-target='#detail$data[id_inventaris]' href='' 
				class='btn btn-info'>i</a>"; ?>
				<a onclick="return confirm('Apakah anda yakin???')" 
				href="page/barang/proses.php?aksi=hapus&id=<?php echo $data['id_inventaris'] ?>" class="btn btn-danger">x</a>
			</td>	
		</tr>
		<?php
		$no++;
		}
		 ?>
		</tbody>
	</table>
</div>


<?php 
//include "modal.php";
?>
