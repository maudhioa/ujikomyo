<?php 
	include "function.php";
	$z = new database();
	$a = $z->tampil_guru();
?>

<p class="title-content"><b>Tabel Data Guru</b></p>
<div id="hor-line"></div>

<div class="row">
	<div class="col-md-9">

<a class="btn btn-success" href="?page=guru&kej=tambah" class="btn btn-success" style="margin-bottom: 15px;"><b>+</b> Tambah Data Guru</a> 
</div>
<!--<p align="right" style="color: black;"> Buat Laporan :  <a href="" class="btn btn-dark">Ex</a>
<a href="" class="btn btn-dark">Pdf</a></p>-->
</div>

<?php 
include "../koneksi.php";
$modal=$koneksi->query("SELECT * FROM guru");
while ($sq = $modal->fetch_assoc()) 
{
	?>
	<div class="modal fade" id="myModal<?=$sq['id_guru'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h3><b>Data Lengkap Siswa</b></h3><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<div class="table-responsive">
						<table class="table table-bordered table-hover">
							<thead></thead>
							<tbody>
								<?php
								$dataa =$koneksi->query("SELECT * FROM guru 
									WHERE id_guru='$sq[id_guru]' ");
								$no=1;
								while ($ra=$dataa->fetch_assoc())
								{														
									echo"<tr>";
									echo '<th>Nama</th>';
									echo "<td>$ra[nama_guru]</td>";
									echo"</tr>";

									echo"<tr>";
									echo'<th>NIS</th>';
									echo "<td>$ra[nip]</td>";
									echo"</tr>";

									echo"<tr>";
									echo'<th>Alamat</th>';			
									echo "<td>$ra[alamat]</td>";
									echo"</tr>";

									echo"<tr>";
									echo"<th colspan=2><a class='btn btn-dark' style='color:white' href='?page=guru&kej=ubah&id_guru=$ra[id_guru]'>Edit</a></th>";			
									echo"</tr>";
									$no++;
								}
								?>
							</tbody>
						</table>
					</div>		
				</div>
			</div>
		</div>
	</div>
<?php } ?>

<div class="table-responsive">
	<table class="table table-bordered table-hover" id="example">
		<thead>
			<tr>
				<th>No</th>
				<th>Nama Guru</th>
				<th>NIP</th>
				<th>Alamat</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
		<?php
		$no=1; 
		foreach ($a as $data) {
		?>
		<tr>
			<td><?php echo $no ?></td>	
			<td><?php echo $data['nama_guru'] ?></td>	
			<td><?php echo $data['nip'] ?></td>	
			<td><?php echo $data['alamat'] ?></td>	
			<td>
				<?php echo "
				<a data-toggle='modal' data-target='#myModal$data[id_guru]'
				class='btn btn-info'><b>i</b></a> "; ?>
				<a onclick="return confirm('Apakah anda yakin???')" 
				href="page/guru/proses.php?aksi=hapus_guru&id=<?php echo $data['id_guru'] ?>" class="btn btn-danger">X</a>
			</td>	
		</tr>
		<?php
		$no++;
		}
		 ?>
		</tbody>
	</table>
</div>


<?php 
include "modal.php";
?>
