<?php 
	include "function.php";
	$aksi = $_GET['aksi'];
	$db = new database();

	if($aksi=="hapus_guru"){
 		$db->hapus_guru($_GET['id']);
	 	if ($db) {
	 		?>
		 	<script type="text/javascript">
				alert("Data Berhasil dihapus");
				window.location.href="../../?page=guru";
			</script>
	 	<?php
	 	}else{
	     	echo "HAPUS GAGAL";
	     }
 	}
 	if ($aksi=="tambah_guru") {
 		$db->tambah_guru($_POST['nama'],$_POST['nip'],$_POST['alamat']);
	 	if ($db) {
	 		?>
		 	<script type="text/javascript">
				alert("Data Berhasil ditambah");
				window.location.href="../../?page=guru";
			</script>
	 	<?php
	 	}else{
	     	echo "INPUT GAGAL";
	     }
 	}
 	if ($aksi=="ubah_guru") {
 		$db->ubah_guru($_POST['id_guru'],$_POST['nama'],$_POST['nip'],$_POST['alamat']);
	 	if ($db) {
	 		?>
		 	<script type="text/javascript">
				alert("Data Berhasil diubah");
				window.location.href="../../?page=guru";
			</script>
	 	<?php
	 	}else{
	     	echo "INPUT GAGAL";
	     }
 	}

?>