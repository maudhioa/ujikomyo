<?php 
include "function.php";
$data = new database();

?>


<p align="center" id="title-laporan">DATA PEMINJAMAN BARANG</p><br>
<a type="button" value="EXCEL" href="" id="cetak" class="no-print"> </a>

<table border="1" width="100%" style="border-collapse: collapse;">
	<thead class="title-table">
		<tr style="height: 40px;">
			<th>No</th>
			<th>Nama Peminjam</th>
			<th>Level</th>
			<th>Barang</th>
			<th>Jumlah</th>
			<th>Tgl. Pinjam</th>
			<th>Tgl. Kembali</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		$no=1;
		$tampil = $data->pengembalian_lengkap("TELAH DIKEMBALIKAN"); 
		foreach ($tampil as $a) {
		?>
		<tr style="height: 30px; text-align: center;">
			<td><?php echo $no; ?></td>
			<td><?php echo $a["nama_peminjam"]; ?></td>
			<td><?php echo $a["level"]; ?></td>
			<td><?php echo $a["nama"]; ?></td>
			<td><?php echo $a["jumlah"]; ?></td>
			<td><?php echo $a["tanggal_pinjam"]; ?></td>
			<td><?php echo $a["tanggal_kembali"]; ?></td>
		</tr>
		<?php 	
		 $no++;
		 } 
		 ?>
	</tbody>
</table>




<?php

header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=data-barang.xls");
 
// Tambahkan table
?>