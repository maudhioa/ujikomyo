<?php 
include "function.php";
$tampil = new database();
$a = $tampil->tampil_operator();

?>

<p class="title-content"><b>Tabel Data Operator</b></p>
<div id="hor-line"></div>

<div class="row">
	<div class="col-md-9">
<a data-toggle="modal" data-target="#TambahModal" href="" class="btn btn-success" style="margin-bottom: 15px;"><b>+</b> Tambah Operator</a> 
</div>
<!--<p align="right" style="color: black;"> Buat Laporan :  <a href="" class="btn btn-dark">Ex</a>
<a href="" class="btn btn-dark">Pdf</a></p>-->
</div>

<?php 
include "../koneksi.php";
$modal=$koneksi->query("SELECT * FROM petugas p JOIN level l ON p.id_lvl=l.id_level");
while ($sq = $modal->fetch_assoc()) 
{
	?>
	<div class="modal fade" id="myModal<?=$sq['id_petugas'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h3><b>Data Lengkap Operator</b></h3><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<div class="table-responsive">
						<table class="table table-bordered table-hover">
							<thead></thead>
							<tbody>
								<?php
								$dataa =$koneksi->query("SELECT * FROM petugas p JOIN level l ON p.id_lvl=l.id_level WHERE id_petugas=$sq[id_petugas]" );
								$no=1;
								while ($ra=$dataa->fetch_assoc())
								{														
									echo"<tr>";
									echo '<th>Nama Petugas</th>';
									echo "<td>$ra[nama_petugas]</td>";
									echo"</tr>";

									echo"<tr>";
									echo'<th>Username</th>';
									echo "<td>$ra[username]</td>";
									echo"</tr>";

									echo"<tr>";
									echo'<th>Password</th>';			
									echo "<td>$ra[password]</td>";
									echo"</tr>";

									echo"<tr>";
									echo'<th>Level</th>';			
									echo "<td>$ra[nama_level]</td>";
									echo"</tr>";

									echo"<tr>";
									echo"<th colspan=2><a class='btn btn-dark' style='color:white' href='?page=operator&kej=ubah&id_petugas=$ra[id_petugas]'>Edit</a></th>";			
									echo"</tr>";
									$no++;
								}
								?>
							</tbody>
						</table>
					</div>		
				</div>
			</div>
		</div>
	</div>
<?php } ?>



<div class="table-responsive">
	<table class="table table-bordered table-hover" id="example">
		<thead>
			<tr>
				<th>No</th>
				<th>Nama Petugas</th>
				<th>Level</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
		<?php
		$no=1; 
		foreach ($a as $data) {
		?>
		<tr>
			<td><?php echo $no ?></td>	
			<td><?php echo $data['nama_petugas'] ?></td>	
			<td><?php echo $data['nama_level'] ?></td>	
			<td>
				<?php echo "
				<a data-toggle='modal' data-target='#myModal$data[id_petugas]'
				class='btn btn-info'><b>i</b></a> "; ?>
				<a onclick="return confirm('Apakah anda yakin???')" 
				href="page/operator/proses.php?aksi=hapus_petugas&id=<?php echo $data['id_petugas'] ?>" class="btn btn-danger">X</a>
			</td>	
		</tr>
		<?php
		$no++;
		}
		 ?>
		</tbody>
	</table>
</div>

<?php 
include "modal.php"; 
?>


