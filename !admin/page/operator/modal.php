
<!-- TAMBAH MODAL -->
<div class="modal fade" id="TambahModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="smallModalLabel" style="color: #222;">Tambah Petugas</h4>
      </div>
      <form class="form-horizontal" method="POST" action="page/operator/proses.php?aksi=tambah_petugas">
        <div class="modal-body" style="color: #222;">
          <div class="form-group">
            <label>Nama Petugas</label>
            <input type="text" name="nama_petugas" class="form-control" autocomplete="off">
          </div>
          <!--<div class="form-group">
            <label>Level</label>
            <select class="form-control" name="id_lvl">
              <?php
              include "koneksi.php";
              $query=$koneksi->query("SELECT * FROM level");
              while ($sql=$query->fetch_assoc()) {
                echo"
                <option value=$sql[id_level]>$sql[nama_level]</option>                 
                ";
              }               
              ?>
            </select>
          </div>-->
          <div class="form-group">
            <label>Username</label>
            <input type="text" name="username" class="form-control" autocomplete="off">
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="password" name="password" class="form-control" autocomplete="off">
          </div>  
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Tambah</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>


<?php 
include "koneksi.php";
$query=$koneksi->query("SELECT * FROM petugas LEFT JOIN level ON petugas.id_lvl=level.id_level");
while ($sq = $query->fetch_assoc()) 
{
?>

  <div class="modal fade" id="DetailOperator<?= $sq['id_petugas']; ?>" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="smallModalLabel" style="color: #222;">Tambah Petugas</h4>
        </div>
        <div class="modal-body" style="color: #222;">
          <div class="table-responsive">
            <table id="example3">
              <thead></thead>
              <tbody>

                <?php                
                $query=$koneksi->query("SELECT * FROM petugas LEFT JOIN level ON petugas.id_lvl=level.id_level WHERE id_petugas='$sq[id_petugas]' ");
                $no=1;
                while ($ra=$query->fetch_assoc())
                {
                  echo"<tr>";
                  echo'<th>Nama Petugas</th>';
                  echo"<td>:</td>";
                  echo "<td>$ra[nama_petugas]</td>";
                  echo'</tr>';

                  echo"<tr>";
                  echo '<th>Level</th>';
                  echo"<td>:</td>";
                  echo "<td>$ra[nama_level]</td>";
                  echo"</tr>";

                  echo"<tr>";
                  echo '<th>Username</th>';
                  echo"<td>:</td>";
                  echo "<td>$ra[username]</td>";
                  echo"</tr>";

                  echo"<tr>";
                  echo '<th>Password</th>';  
                  echo"<td>:</td>";
                  echo "<td>$ra[password]</td>";
                  echo"</tr>";                   
                  $no++;
                }
                ?>
              </tbody>
            </table>
          </div>

        </div>
      </div>
    </div>
    <?php
  };
  ?>