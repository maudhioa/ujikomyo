<?php 
include "koneksi.php";
$sql = $koneksi->query("SELECT * FROM petugas p LEFT JOIN level l ON p.id_lvl=l.id_level WHERE id_petugas = $_SESSION[id_admin]");
$tampil = $sql->fetch_assoc();


?>

<p class="title-content"><b>Profile Pengguna/Akun</b></p>
<div id="hor-line"></div>

<style type="text/css">
	#td__profile{
		background-color: #f8f9fa;
		width: 30%;
	}
</style>

<div class="table-responsive" style="width: 70%;">
<table class="table table-bordered table-hover">
		<tr>
			<td id="td__profile">Nama Pengguna</td>
			<td><?php echo $tampil['nama_petugas']; ?></td>
		</tr>
		<tr>
			<td id="td__profile">Level</td>
			<td><?php echo $tampil['nama_level']; ?></td>
		</tr>
		<tr>
			<td id="td__profile">Username</td>
			<td><?php echo $tampil['username']; ?></td>
		</tr>
		<tr>
			<td id="td__profile">Password</td>
			<td><?php echo $tampil['password']; ?></td>
		</tr>		
		<tr>
			<td colspan="2"><a href="" data-toggle="modal" data-target="#EditModal" class="btn btn-info">Edit</a></td>
		</tr>
</table>
</div>


<div class="modal fade" id="EditModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="smallModalLabel" style="color: #222;">Edit Profile</h4>
      </div>
      <form class="form-horizontal" method="POST" action="page/akun/proses.php?aksi=edit_profile">
        <div class="modal-body" style="color: #222;">
          <div class="form-group">
            <label>Nama Petugas</label>
            <input type="text" name="nama_petugas" value="<?php echo $tampil['nama_petugas']; ?>" class="form-control" autocomplete="off">
            <input type="hidden" name="id_petugas" value="<?php echo $tampil['id_petugas']; ?>">
          </div>
          <div class="form-group">
            <label>Level</label>
            <select class="form-control" name="id_lvl">
              <?php
              include "koneksi.php";
              $query=$koneksi->query("SELECT * FROM level");
              while($sql=$query->fetch_assoc()) {
                ?>
                <option <?php if($tampil["id_lvl"]==$sql["id_level"]) echo "selected"; ?> value=<?php echo $sql["id_level"] ?>> <?php echo $sql["nama_level"] ?></option>                 
                <?php ;
              }               
              ?>
            </select>
          </div>
          <div class="form-group">
            <label>Username</label>
            <input type="text" name="username" value="<?php echo $tampil['username']; ?>" class="form-control" autocomplete="off">
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="password" name="password" value="<?php echo $tampil['password']; ?>" class="form-control" autocomplete="off">
          </div>  
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Ubah</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>