<?php 
include "function.php";
$tampil = new database();
$a = $tampil->tampil_operator();

?>

<p class="title-content"><b>Tabel Data Ruang</b></p>
<div id="hor-line"></div>

<div class="row">
	<div class="col-md-9">
<a data-toggle="modal" data-target="#TambahModal" href="" class="btn btn-success" style="margin-bottom: 15px;"><b>+</b> Tambah Ruang</a> 
</div>
<p align="right" style="color: black;"> Buat Laporan :  <a href="" class="btn btn-dark">Ex</a>
<a href="" class="btn btn-dark">Pdf</a></p>
</div>


<div class="table-responsive">
	<table class="table table-bordered table-hover" id="example">
		<thead>
			<tr>
				<th>No</th>
				<th>Nama Ruang</th>
				<th>Kode</th>
				<th>Keterangan</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
		<?php
		$no=1; 
		foreach ($a as $data) {
		?>
		<tr>
			<td><?php echo $no ?></td>	
			<td><?php echo $data['nama_ruang'] ?></td>	
			<td><?php echo $data['kode_ruang'] ?></td>	
			<td><?php echo $data['keterangan'] ?></td>	
			<td>
				<a class="btn btn-info" href="?page=ruang&kej=ubah&id=<?php echo $data['id_ruang'] ?>">E</a>
				<a onclick="return confirm('Apakah anda yakin???')" 
				href="page/ruang/proses.php?aksi=hapus&id=<?php echo $data['id_ruang'] ?>" class="btn btn-danger">X</a>
			</td>	
		</tr>
		<?php
		$no++;
		}
		 ?>
		</tbody>
	</table>
</div>

<div class="modal fade" id="TambahModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="smallModalLabel" style="color: #222;">Tambah Petugas</h4>
      </div>
      <form class="form-horizontal" method="POST" action="page/ruang/proses.php?aksi=tambah">
        <div class="modal-body" style="color: #222;">
          <div class="form-group">
            <label>Nama Ruang</label>
            <input type="text" name="nama_ruang" class="form-control" autocomplete="off">
          </div>
          <div class="form-group">
            <label>Kode Ruang</label>
            <input type="text" name="kode_ruang" class="form-control" autocomplete="off">
          </div>
          <div class="form-group">
            <label>Keterangan</label>
            <textarea class="form-control" name="keterangan"></textarea>
          </div>  
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Tambah</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>


