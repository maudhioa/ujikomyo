<?php 
	include "function.php";
	$aksi = $_GET['aksi'];
	$db = new database();

	if($aksi=="hapus_ruang"){
 		$db->hapus_siswa($_GET['id']);
	 	if ($db) {
	 		?>
		 	<script type="text/javascript">
				alert("Data Berhasil dihapus");
				window.location.href="../../?page=siswa";
			</script>
	 	<?php
	 	}
 	}

 	if($aksi=="tambah") {
 		$db->tambah_ruang($_POST['nama_ruang'],$_POST['kode_ruang'],$_POST['keterangan']);
 		if ($db) {
 			?>
		 	<script type="text/javascript">
				alert("Data Berhasil ditambah");
				window.location.href="../../?page=ruang";
			</script>
	 	<?php
 		}
 	}

 	if ($aksi=="ubah") {
 		$db->ubah_ruang($_POST['nama'],$_POST['kode'],$_POST['keterangan'],$_POST['id_ruang']);
 		if ($db){
 			?>
		 	<script type="text/javascript">
				alert("Data Berhasil diubah");
				window.location.href="../../?page=ruang";
			</script>
	 	<?php
 		}
 	}

 	if ($aksi=="hapus") {
 		$db->hapus_ruang($_GET['id']);
 		if ($db){
 			?>
		 	<script type="text/javascript">
				alert("Data Berhasil dihapus");
				window.location.href="../../?page=ruang";
			</script>
	 	<?php
 		}
 	}
?>