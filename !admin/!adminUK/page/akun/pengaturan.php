<p class="title-content"><b>Pengaturan Akun</b></p>
<div id="hor-line"></div>

<div>
<label>Pilih Bahasa :</label><br>
	<a href="../"><button class="btn btn-danger">INDONESIA</button></a>
	<a href="!adminUK"><button class="btn btn-primary">ENGLISH</button></a>
</div>

<label>Pilih Tema :</label><br>
<form class="form-horizontal" action="" method="POST">
<select name="tema" class="form-control">
	<option>
		<button class="btn btn-dark" value="onyx">Onyx</button>
	</option>
	<option>
		<button class="btn btn-success" value="green">Green</button>
	</option>
</select>
<input class="btn" type="submit" name="submit" value="Ubah">
</form>

<?php 
if (isset($_POST['submit'])) {
 	
 	$tema=$_POST['tema'];

 	if ($tema=="onyx") {
 		include "<link rel='stylesheet' type='text/css' href='tema-onyx.css'>";
 	}

 } ?>



<script type="text/javascript">
	function warna (pilih) {
		if (pilih == 1) document.body.style.backgroundColor = "red";
		else if (pilih == 2) document.body.style.backgroundColor = "yellow";
		else if (pilih == 3) document.body.style.backgroundColor = "green";
		else if (pilih == 4) document.body.style.backgroundColor = "blue";
	}
</script>