<p class="title-content"><b>Tambah Data Jenis</b></p>
<div id="hor-line"></div>

<form class="form-horizontal" method="POST" action="page/jenis/proses.php?aksi=tambah" enctype="multipart/form-data"> 
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label>Nama Jenis * :</label>
				<input type="text" name="nama" class="form-control" autocomplete="off" required>
			</div>
			<div class="form-group">
				<label>Kode Jenis * :</label>
				<input type="text" name="kode" class="form-control" autocomplete="off" required>
			</div>
			<div class="form-group">
				<label>Keterangan</label>
				<textarea name="keterangan" class="form-control"></textarea>
			</div>
		</div>
	</div><br>
	<button type="submit" class="btn btn-success">Tambah</button>
	<button type="button" class="btn btn-danger"><a href="?page=siswa">kembali</a></button>
</form>