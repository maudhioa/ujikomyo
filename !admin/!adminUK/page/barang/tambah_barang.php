<?php 
	session_start();
?>
<p class="title-content"><b>Tambah Data Barang / Inventaris</b></p>
<div id="hor-line"></div>

<form class="form-horizontal" method="POST" action="page/barang/proses.php?aksi=tambah_barang" enctype="multipart/form-data"> 
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label>Nama Barang * :</label>
				<input type="text" name="nama_barang" class="form-control" autocomplete="off" required>
			</div>
			<div class="form-group">
				<label>Kode * :</label>
				<input type="text" name="kode" class="form-control" autocomplete="off" required>
			</div>
			<div class="form-group">
				<label>Jumlah * :</label>
				<input type="number" name="jumlah" class="form-control" autocomplete="off" required>
			</div>
			<div class="form-group">
				<label>Kondisi * :</label>
				<select name="kondisi" class="form-control">
					<option>Buruk</option>
					<option selected>Baik</option>
					<option>Sangat Baik</option>
				</select>
				<input type="hidden" name="id_petugas" value="<?php echo $_SESSION['id_admin']; ?>">
			</div>
	
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label>Ruang * :</label>
				<select class="form-control" name="ruang" required>
					<?php
					include "koneksi.php";
					$query=$koneksi->query("SELECT * FROM ruang");
					while($sql=$query->fetch_assoc()) {
					?>
						<option value=<?php echo $sql["id_ruang"] ?>> <?php echo $sql["nama_ruang"] ?></option>                 
					<?php ;
					}               
					?>
				</select>
			</div>
			<div class="form-group">
				<label>Jenis * :</label>
				<select class="form-control" name="jenis" required>
					<?php
					include "koneksi.php";
					$query=$koneksi->query("SELECT * FROM jenis");
					while($sql=$query->fetch_assoc()) {
					?>
						<option value=<?php echo $sql["id_jenis"] ?>> <?php echo $sql["nama_jenis"] ?></option>                 
					<?php ;
					}               
					?>
				</select>
			</div>
				<label>Keterangan :</label>
				<textarea class="form-control" name="keterangan"></textarea>
				<label>Foto/Gambar * :</label>
				<input class="form-control" type="file" name="foto">
		</div>
	</div>
	<button type="submit" class="btn btn-success">Tambah</button>
	<button type="button" class="btn btn-danger"><a href="?page=barang">kembali</a></button>
</form>